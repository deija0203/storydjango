from django.shortcuts import render

# Create your views here.

def buku(request):
    return render(request, 'buku.html')

def index(request):
    return render(request, 'index.html')

def deijaprofile(request):
    return render(request, 'deijaprofile.html')
