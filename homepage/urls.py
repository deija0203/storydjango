from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.buku, name='buku'),
    path('profile/', views.index, name='index'),
    path('oldProfile/', views.deijaprofile, name='deijaprofile'),
]
